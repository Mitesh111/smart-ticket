package com.example.mk.iot;

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.TextureView;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    private TextView lat,lon;
    private double a,b;
    private String url="http://trackyourbus1.000webhostapp.com/fetch.php";
    private Map<String,String> map;
    private Button button;
    String address;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        lat=findViewById(R.id.latitude);
        lon=findViewById(R.id.longitude);
        final RequestQueue requestQueue =Volley.newRequestQueue(this);
        button=findViewById(R.id.btn);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                     a = response.getDouble("a");
                     b = response.getDouble("b");
                    lat.setText(""+a);
                    lon.setText(""+b);
                    Log.v("errorblah","onResponse");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.v("errorblah",error.getMessage());
            }
        });


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Geocoder geocoder=new Geocoder(getApplicationContext(), Locale.getDefault());
                try {
                    double l1 = a;
                    double l2 = b;
                    List<Address> addresses = geocoder.getFromLocation(a,b,1);
                    Toast.makeText(getApplicationContext(),"size "+addresses.size(),Toast.LENGTH_SHORT).show();
                    if(addresses.size()>0) {
                        Address obj = addresses.get(0);
                        address = obj.getAddressLine(0) + " " + obj.getLocality() + " " + obj.getPremises() + " " + obj.getAdminArea();
                    }

                }
                catch(IOException e){
                    e.printStackTrace();
                }
                String urlAddress = "http://maps.google.com/maps?q="+ a +"," + b +"("+address+")&iwloc=A&hl=es";
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(urlAddress));
                startActivity(intent);
            }
        });

        requestQueue.add(jsonObjectRequest);
    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
